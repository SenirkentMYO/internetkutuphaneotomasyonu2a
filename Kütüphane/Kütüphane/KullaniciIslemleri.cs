﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Kütüphane
{
    public class KullaniciIslemleri
    {
        public Kullanici Giris(string email, string sifre)
        {
            List<Kullanici> liste = new List<Kullanici>();
            SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["baglantiCumlesi"].ToString());
            baglanti.Open();
            SqlCommand komut = new SqlCommand();
            komut.Connection = baglanti;
            komut.CommandType = CommandType.StoredProcedure;//Komut Türünü belirliyoruz
            komut.CommandText = "GirisSP";//Veri Tabanındaki Stored Procedure ismi

            SqlParameter p1 = new SqlParameter();
            p1.DbType = DbType.String;
            p1.ParameterName = "@GelenEmail";
            p1.Value = email;

            SqlParameter p2 = new SqlParameter();
            p2.DbType = DbType.String;
            p2.ParameterName = "@GelenSifre";
            p2.Value = sifre;

            komut.Parameters.Add(p1);
            komut.Parameters.Add(p2);

            SqlDataReader satir = komut.ExecuteReader();




            while (satir.Read())
            {
                Kullanici k = new Kullanici();
                k.Email = (string)satir["Email"];
                k.Sifre = (string)satir["Sifre"];
                k.Ad = (string)satir["Ad"];

                liste.Add(k);

            }
            baglanti.Close();//Bağlantıyı kapatır

            if (liste.Count > 0)
            {
                return liste[0];
            }
            else
            {
                return null;
            }




        }
    }
}