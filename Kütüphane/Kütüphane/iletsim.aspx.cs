﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kütüphane
{
    public partial class iletsim : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["GirenUye"] == null)
            {
                Response.Redirect("Default.aspx");


            }
            else
            {

                lbnGiris.Text = "Hoşgeldin " + ((Kullanici)Session["GirenUye"]).Ad + " " + ((Kullanici)Session["GirenUye"]).Soyad;
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("iletsim.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("KitapAra2.aspx");
        }

        protected void txtHak_Click(object sender, EventArgs e)
        {
            Response.Redirect("Hakkimizda.aspx");
        }

        protected void btnCikis_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}