﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kütüphane
{
    public partial class KitapAra2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["GirenUye"] == null)
            {
                Response.Redirect("Default.aspx");


            }
            else
            {

                lbnGiris.Text = "Kitap Aramaya Hoşgeldin " + ((Kullanici)Session["GirenUye"]).Ad + " " + ((Kullanici)Session["GirenUye"]).Soyad;
            }
        }

       
           
        

        protected void btnAnasayfa_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

            protected void btnCikis_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
       

        protected void txtAnasayfa_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        

        protected void btnUye_Click(object sender, EventArgs e)
        {
            Response.Redirect("uyekaydi.aspx");
        }

        protected void btnAra_Click(object sender, EventArgs e)
        {


            List<VwKitapYazari> listKullanici = new List<VwKitapYazari>();
            SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["baglantiCumlesi"].ToString());

            baglanti.Open();
            string sorgu = "Select * from VwKitapYazari where KitapAdi ='" + txtKitapAdi.Text + "'or YazarAdi='" + txtYazarAd.Text + "'";

            SqlCommand komut = new SqlCommand(sorgu, baglanti);


            SqlDataReader dr = komut.ExecuteReader();
            while (dr.Read())
            {
                VwKitapYazari k = new VwKitapYazari();

                k.KitapAdi = (string)dr["KitapAdi"];
                k.YazarAdi = (string)dr["YazarAdi"];
                k.YazarSoyadi = (string)dr["YazarSoyadi"];
                k.KayitTarihi = (DateTime)dr["KayitTarihi"];


                listKullanici.Add(k);


            }
            baglanti.Close();
            baglanti.Dispose();

            if (listKullanici.Count > 0)
            {

                lblmesaj.Text = "";

                Repeater1.DataSource = listKullanici;
                Repeater1.DataBind();
            }
            else
            {
                if (txtKitapAdi.Text == "")
                {
                    lblmesaj.Text = "Kütüphanemizde - " + txtYazarAd.Text + " - kişisine ait kitap bulunmamaktadır";
                    Repeater1.DataSource = listKullanici;
                    Repeater1.DataBind();
                }
                else
                    lblmesaj.Text = "Kütüphanemizde - " + txtKitapAdi.Text + " - adında kitap bulunmamaktadır";
                Repeater1.DataSource = listKullanici;
                Repeater1.DataBind();
            }

        }

        protected void btnKitapAra_Click(object sender, EventArgs e)
        {
           
        }

        protected void btnİletisim_Click(object sender, EventArgs e)
        {
            Response.Redirect("iletsim.aspx");
        }

        protected void txtHak_Click(object sender, EventArgs e)
        {
            Response.Redirect("Hakkimizda.aspx");
        }
    }
}