﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kütüphane
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Application["sayac"] == null)
            {
                Application["sayac"] = 1;
                lblZiyaretci.Text = Application["sayac"].ToString();
            }
            else
            {
                int sayi = (int)Application["sayac"];
                sayi++;
                Application["sayac"] = sayi;
                lblZiyaretci.Text = Application["sayac"].ToString();
            }
        }

      
           

        protected void btnUye_Click(object sender, EventArgs e)
        {
            Response.Redirect("uyekaydi.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            KullaniciIslemleri ki = new KullaniciIslemleri();
            Kullanici kul = ki.Giris(txtEmail.Text, txtSifre.Text);

            if (kul == null)
            {
                Response.Redirect("Default.aspx");
                lblHata.Text = "Hatalı Giriş";
            }
            else
            {
                Session["GirenUye"] = kul;
                Response.Redirect("index.aspx");

                btnAnasayfa.Enabled = true;
                btnKitapAra.Enabled = true;
                btnİletisim.Enabled = true;

            }
            //try
            //{
            //    List<Kullanici> listKullanici = new List<Kullanici>();


            //    using (SqlConnection baglan = new SqlConnection(ConfigurationManager.ConnectionStrings["baglantiCumlesi"].ToString()))
            //    {
            //        baglan.Open();

            //        string sorguCümlesi = "Select * from Kullanici Where Email='" + txtEmail.Text + "' and Sifre='" + txtSifre.Text + "'";
            //        System.Data.SqlClient.SqlCommand komut = new SqlCommand(sorguCümlesi, baglan);
            //        komut.ExecuteNonQuery();

            //        System.Data.SqlClient.SqlDataReader satir = komut.ExecuteReader();




            //        while (satir.Read())
            //        {
            //            Kullanici k = new Kullanici();
            //            k.Email = (string)satir["Email"];
            //            k.Sifre = (string)satir["Sifre"];
            //            k.Ad = (string)satir["Ad"];
            //            k.Soyad = (string)satir["Soyad"];

            //            listKullanici.Add(k);




            //        }




            //        baglan.Close();//Bağlantıyı kapatır

            //        if (listKullanici.Count > 0)
            //        {
            //            Session["GirenUye"] = listKullanici[0];
                      
            //            Response.Redirect("index.aspx");
                       
            //            btnAnasayfa.Enabled = true;
            //            btnKitapAra.Enabled = true;
            //            btnİletisim.Enabled = true;
                       
            //           }

            //        else
            //        {
            //            Session["GirenUye"] = null;
            //            lblHata.Text = "Mail veya Şifre Hatalı!!!";
                       
            //        }


            //    }





            //}
            //catch (Exception ex)
            //{
            //    Response.Write("hata;" + ex.ToString());
            //}
        }

        protected void btnAnasayfa_Click(object sender, EventArgs e)
        {
            Response.Redirect("KitapAra2.aspx");
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {

        }

        protected void btnCikis_Click(object sender, EventArgs e)
        {
            lbnGiris.Text = "Lütfen linklere erişebilmek için giriş yapınız !!!";
            btnAnasayfa.Enabled = false;
            btnKitapAra.Enabled = false;
            btnİletisim.Enabled = false;
            
        }

        protected void txtAnasayfa_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void Button1_Click2(object sender, EventArgs e)
        {
            txtEmail.Text = "";
            txtSifre.Text = "";
        }

        protected void btnİletisim_Click(object sender, EventArgs e)
        {
            Response.Redirect("iletsim.aspx");
        }
    }
}