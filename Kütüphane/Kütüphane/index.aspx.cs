﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kütüphane
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["GirenUye"] == null)
            {
                Response.Redirect("Default.aspx");


            }
            else
            {

                lbnGiris.Text = "Hoşgeldin " + ((Kullanici)Session["GirenUye"]).Ad + " " + ((Kullanici)Session["GirenUye"]).Soyad;
            }
            try
            {
                List<VwKitapYazari> listKitap = new List<VwKitapYazari>();


                using (SqlConnection baglan = new SqlConnection(ConfigurationManager.ConnectionStrings["baglantiCumlesi"].ToString()))
                {
                    baglan.Open();

                    string sorguCümlesi = "Select * from VwKitapYazari";
                    System.Data.SqlClient.SqlCommand komut = new SqlCommand(sorguCümlesi, baglan);
                    komut.ExecuteNonQuery();

                    System.Data.SqlClient.SqlDataReader satir = komut.ExecuteReader();




                    while (satir.Read())
                    {
                        VwKitapYazari k = new VwKitapYazari();
                        k.KitapID = (int)satir["KitapID"];
                        k.KitapAdi = (string)satir["KitapAdi"];
                        k.YazarAdi = (string)satir["YazarAdi"];
                        k.YazarSoyadi = (string)satir["YazarSoyadi"];
                        k.KayitTarihi= (DateTime)satir["KayitTarihi"];

                        listKitap.Add(k);




                    }

                    baglan.Close();
                    baglan.Dispose();
                    DataList1.DataSource = listKitap;
                    DataList1.DataBind();

                    //Bağlantıyı kapatır




                }





            }
            catch (Exception ex)
            {
                Response.Write("hata;" + ex.ToString());
            }
        
    

        }

        protected void btncikis_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

       
       
        protected void btnKitapAra_Click(object sender, EventArgs e)
        {
            Response.Redirect("KitapAra2.aspx");
        }

        protected void btnİletisim_Click(object sender, EventArgs e)
        {
            Response.Redirect("iletsim.aspx");
        }

        protected void btnCikis_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void btnAra_Click(object sender, EventArgs e)
        {

        }

        protected void btnAnasayfa_Click(object sender, EventArgs e)
        {

        }

        protected void txtHak_Click(object sender, EventArgs e)
        {
            Response.Redirect("Hakkimizda.aspx");
        }

        
    }
}