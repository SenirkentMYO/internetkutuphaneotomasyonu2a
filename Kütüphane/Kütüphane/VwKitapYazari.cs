﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kütüphane
{
    public class VwKitapYazari
    {

        public int KitapID { get; set; }
        public string KitapAdi { get; set; }         
        public string YazarAdi { get; set; }
        public string YazarSoyadi { get; set; }
        public  DateTime KayitTarihi { get; set; }
      
        

    }
}