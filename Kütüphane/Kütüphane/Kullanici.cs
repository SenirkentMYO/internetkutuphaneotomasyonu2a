﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kütüphane
{
    public class Kullanici
    {
        public int ID
        {
            get;
            set;

        }
        // private string _Adi;
        public string Ad
        {
            get;
            set;

        }
        public string Soyad
        {
            get;
            set;

        }
        public string Email
        {
            get;
            set;

        }
        public string Sifre
        {
            get;
            set;

        }
        public int RolID
        {
            get;
            set;

        }
        public int KayitEdenID
        {
            get;
            set;

        }
        public DateTime KayitTarihi
        {
            get;
            set;

        }
        public int DegistirenID
        {
            get;
            set;

        }
        public DateTime DegisiklikTarihi
        {
            get;
            set;

        }
       
      
    }
}